@echo off

set DDMMYY=%date:~7,2%%date:~4,2%%date:~10,4%
echo date
echo %DDMMYY%
echo Making Folder
mkdir %DDMMYY%
echo searching and copying files in drive
for /R "d:\" %%G in (*.nef *.jpg *.jpeg *.tif *.tiff *.dng) do copy "%%G" ".\%DDMMYY%"
echo compressing folder
7z a %DDMMYY% .\%DDMMYY%\*
echo deleting folder
rmdir  %DDMMYY% /s /q
echo done 
pause
@echo off
set DDMMYY=%date:~7,2%%date:~4,2%%date:~10,4%
echo %DDMMYY%
mkdir %DDMMYY%
for /R "d:\" %%G in (*.nef *.jpg *.jpeg *.tif *.tiff *.dng) do copy "%%G" ".\%DDMMYY%"
7z a %DDMMYY% .\%DDMMYY%\*
rmdir  %DDMMYY% /s /q